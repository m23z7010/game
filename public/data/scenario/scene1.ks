[_tb_system_call storage=system/_scene1.ks]

[cm  ]
[bg  storage="黑地道/牢房关.jpg"  time="1000"  ]
[tb_show_message_window  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-368"  top="-218"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
あなたが私を救った時、私はあなたを置き去りにすることはありません！[p]


[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
私は初めて光を見て、ダンジョン以外の世界を見て、初めてあなたが言った果物を食べて、初めて花を見ました。。。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
外の世界は毎分毎秒私の目を刺す、ありがとう。。。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
だからこそ、私は戻ってくる！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
急げ！間に合わないから、急いで出て行きましょう！[p]


[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
私は鍵を手に入れた、あなたは私たちが一緒に出かける準備ができている！[p]
[_tb_end_text]

[glink  color="pink"  storage="scene1.ks"  size="20"  text="あなたはかわいい！"  x="536"  y="309"  width="110"  height="20"  _clickable_img=""  target="*你好可爱分支"  ]
[glink  color="gray"  storage="scene1.ks"  size="20"  text="よし！"  x="536"  y="243"  width="110"  height="20"  _clickable_img=""  target="*好！分支"  ]
[s  ]
*你好可爱分支

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
え？あなたは突然何と言いましたか。行け！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/牢房开.jpg"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="330"  top="-190"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#士兵
あなたは誰ですか？！！！[p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[mask_off  time="1000"  effect="fadeOut"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  reflect="false"  left="-1324"  top="-790"  width="3836"  height="2773"  ]
[tb_start_text mode=1 ]
#？？？
めまいは正常です[p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[tb_hide_message_window  ]
[s  ]
*好！分支

[chara_show  name="莉莉露"  time="1000"  wait="true"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
さっきから。。説明が間に合わない、ドアが開いた、行こう！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/牢房开.jpg"  ]
[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[mask_off  time="1000"  effect="fadeOut"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/06m.jpg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
出てきて、看守の地図を見たことがありますが、左右左にまっすぐ行って入り口に着いたようです。[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="左"  x="192"  y="300"  width=""  height=""  _clickable_img=""  target="*左1"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="前"  x="599"  y="74"  width=""  height=""  _clickable_img=""  target="*前1"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="1012"  y="295"  width=""  height=""  _clickable_img=""  target="*右1"  ]
[s  ]
*右1

[bg  time="1000"  method="crossfade"  storage="黑地道/黑地道.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-712"  top="-522"  width="2629"  height="1899"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#士兵
!!![p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[s  ]
*前1

[bg  time="1000"  method="crossfade"  storage="黑地道/黑地道.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-896"  top="-628"  width="3043"  height="2199"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#士兵
!!![p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[s  ]
*左1

[bg  time="1000"  method="crossfade"  storage="黑地道/仅右拐.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="976"  y="269"  width=""  height=""  _clickable_img=""  target="*右2"  ]
[s  ]
*右2

[bg  time="1000"  method="crossfade"  storage="黑地道/左右.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="左"  x="174"  y="290"  width=""  height=""  _clickable_img=""  target="*左3"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="978"  y="291"  width=""  height=""  _clickable_img=""  target=""  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[s  ]
*左3

[bg  time="1000"  method="crossfade"  storage="黑地道/06m.jpg"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
着きましたか。[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
前だ、私たちは彼が来る前に間に合わなければならない！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/门关闭.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开启.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开光.jpg"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[tb_hide_message_window  ]
[tb_show_message_window  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[tb_start_text mode=1 ]
#？？？
ここまでだ！[p]
[_tb_end_text]

[chara_show  name="圣子"  time="1000"  wait="true"  left="-77"  top="-268"  width="1394"  height="1008"  reflect="false"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="254"  top="-213"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
さすが君たち、逃げ出すなんて[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
私に会ったからには、すぐに手を縛って捕まえて、さもなくば私たちが遠慮しないのを責めないでください。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
兵士！彼らを取れ！[p]

[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-250"  top="-214"  width=""  height=""  reflect="false"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="197"  top="-189"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
どうしよう、あなたの傷はまだ治っていない、私たちは相手ではありません。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
違う！あなたは、、、、、、[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
あなたは私に鍵をくれたのですね。[p]
[_tb_end_text]

[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="168"  top="-225"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
どうして？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
ふん、ははははははは[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
それはくだらないことではありません。次に条件についてお話ししましょう。。[p]

[_tb_end_text]

[chara_hide  name="莉莉露"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-485"  top="-197"  width=""  height=""  reflect="false"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-188"  top="-225"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
あなたは私を一度だましたことがありますが、今でも二度目のことを考えていますか。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
ふん、私はあなたが自分の今の境遇を知っているべきだと思って、生きたくなくてあなたたちはもちろん拒絶することができて、ただ彼だけを惜しんで、あなたを救うためにこんなに多くしましたか。。。何も変わらない[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
神書に書かれていることは決して変えてはならない。悪魔の女は裁判の日が来るまで弾圧されるに違いない。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
その日は幸せと光が大地を照らす日であり、その日は国を挙げて喜ぶ日であり、その日は------------------------------------------------------------------------------------------[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="-772"  top="-717"  width="2733"  height="1975"  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
私が大司教の座に就いた日！！！[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-172"  top="-226"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
あなたは何をしたいですか。[p]
[_tb_end_text]

[chara_show  name="圣子"  time="1000"  wait="true"  left="159"  top="-230"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
もちろん私の得をしたのです[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
古いものは上に長くいて、持ちすぎて、欲張りすぎて、教会全体のイメージが彼に壊されてしまった！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
教会は改革を迎えなければならない。このままでは、下等な農夫でも一銭もくれないほど愚かではない。神様様は泣くだろう！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
何をしたいの。。。[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
古いものを取り除く[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
。。。私にはわかりません。彼はあなたのお父さんです。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
一人で万人以上になるということを理解していますか。これは面白くありませんし、自慢する価値もありません。もう一人があなたの頭を踏んでいるからです！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
くだらないことを言いすぎて、テーマに入りましょう。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
私たちに何をしてほしいですか。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
何もしなくていい[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
ここから出て、大胆に出て、人を殺すのも、隠すのも、何でもいい。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
悪魔を逃がすほど恐ろしい罪はない。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
ここ数年の宣伝には、人々はとっくに根強い執念を持っている。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
悪魔の脱出、災いの降臨、私のビジネス。のははははははは[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
余計なことは知らなくてもいいですが、もう一つあります。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
これをくれ[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
何を言っているのか分からない[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
地獄のドアを開ける鍵、伝説の悪魔の手の中で、あなたは悪魔ではありませんか？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
私は今までそうではありません！！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
おかしいな[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
あなたは悪魔で、あなたは私をだますことができません！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
私は違います！何を言っているのか分からない！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
兵士、ここには状況がわからない人がいるようだ。[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="銀色の宝石を差し出す"  x="533"  y="232"  width=""  height=""  _clickable_img=""  target="*交出银色宝石分支"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="彼女の前に立ちはだかる"  x="543"  y="315"  width=""  height=""  _clickable_img=""  target="*挡在前面分支"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[s  ]
*交出银色宝石分支

[tb_show_message_window  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-219"  top="-138"  width=""  height=""  reflect="false"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="190"  top="-157"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
あげる[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
賢い人もいる[p]
[_tb_end_text]

[chara_hide  name="圣子"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="223"  top="-173"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
これは？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
あの日、、、出て行った時に拾った、鍵、、、見えた、、、[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
。。。。。。。。。。。。[p]
[_tb_end_text]

[chara_hide  name="莉莉露"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="192"  top="-155"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
鍵は受け取ったから、行ってください。私はあなたたちと無駄話をする暇はありません[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门关闭.jpg"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="176"  top="-133"  width=""  height=""  reflect="false"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-276"  top="-137"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
あなたは私をだました[p]
[_tb_end_text]

[tb_start_text mode=1 ]
少なくとも私たちは出て行ったでしょう、申し訳ありません[p]
[_tb_end_text]

[tb_start_text mode=1 ]
申し訳ありません[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/门开启.jpg"  ]
[tb_start_text mode=1 ]
私たちが生きている限りチャンスがある[p]
[_tb_end_text]

[tb_hide_message_window  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开光.jpg"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[s  ]
*挡在前面分支

[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="-184"  top="-241"  width="1440"  height="1040"  reflect="false"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="58"  top="-248"  width="1614"  height="1167"  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
このようにしましょう、私はまだ多くの方法があって、私は確かに愚か者と交流することができないようです[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
兵士！手を出せ！[p]
[_tb_end_text]

[chara_hide  name="圣子"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_hide_message_window  ]
[chara_hide  name="士兵"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-1135"  top="-757"  width="3514"  height="2538"  reflect="false"  ]
[s  ]
