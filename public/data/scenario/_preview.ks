[_tb_system_call storage=system/_preview.ks ]

[mask time=10]
[mask_off time=10]
[cm  ]
[bg  storage="黑地道/牢房关.jpg"  time="1000"  ]
[tb_show_message_window  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-368"  top="-218"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
为什么，你。。。。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
当初你救了我，我是不会丢下你的[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
快！来不及了，我们要抓紧一切时间出去！[p]


[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
我拿到钥匙了，你准备好我们一起出去！[p]
[_tb_end_text]

[glink  color="pink"  storage="scene1.ks"  size="20"  text="你好可爱！"  x="536"  y="309"  width="110"  height="20"  _clickable_img=""  target="*你好可爱分支"  ]
[glink  color="gray"  storage="scene1.ks"  size="20"  text="好！"  x="536"  y="243"  width="110"  height="20"  _clickable_img=""  target="*好！分支"  ]
[s  ]
*你好可爱分支

[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
啊？你突然间说什么呢？快走！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/牢房开.jpg"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="330"  top="-190"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#士兵
你是谁？！！！[p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[mask_off  time="1000"  effect="fadeOut"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  reflect="false"  left="-1324"  top="-790"  width="3836"  height="2773"  ]
[tb_start_text mode=1 ]
#？？？
头晕是正常的[p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[tb_hide_message_window  ]
[s  ]
*好！分支

[chara_show  name="莉莉露"  time="1000"  wait="true"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
门开了，我们走！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/牢房开.jpg"  ]
[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[mask_off  time="1000"  effect="fadeOut"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/06m.jpg"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#リリル
出来了，我看到过看守的地图，好像是左右左直走后就到门口了。[p]
[_tb_end_text]

[tb_hide_message_window  ]
[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="左"  x="192"  y="300"  width=""  height=""  _clickable_img=""  target="*左1"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="前"  x="599"  y="74"  width=""  height=""  _clickable_img=""  target="*前1"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="1012"  y="295"  width=""  height=""  _clickable_img=""  target="*右1"  ]
[s  ]
*右1

[bg  time="1000"  method="crossfade"  storage="黑地道/黑地道.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-712"  top="-522"  width="2629"  height="1899"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#士兵
!!![p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[s  ]
*前1

[bg  time="1000"  method="crossfade"  storage="黑地道/黑地道.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-896"  top="-628"  width="3043"  height="2199"  reflect="false"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
#士兵
!!![p]
[_tb_end_text]

[mask  time="1000"  effect="fadeIn"  color="0x000000"  ]
[s  ]
*左1

[bg  time="1000"  method="crossfade"  storage="黑地道/仅右拐.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="976"  y="269"  width=""  height=""  _clickable_img=""  target="*右2"  ]
[s  ]
*右2

[bg  time="1000"  method="crossfade"  storage="黑地道/左右.jpg"  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="左"  x="174"  y="290"  width=""  height=""  _clickable_img=""  target="*左3"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="右"  x="978"  y="291"  width=""  height=""  _clickable_img=""  target=""  ]
[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[s  ]
*左3

[bg  time="1000"  method="crossfade"  storage="黑地道/06m.jpg"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  ]
[tb_show_message_window  ]
[tb_start_text mode=1 ]
我们到了吗？[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
就是前面，我们要赶在他来之前！[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/门关闭.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开启.jpg"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开光.jpg"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[tb_hide_message_window  ]
[tb_show_message_window  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[tb_start_text mode=1 ]
#？？？
到此为止了！[p]
[_tb_end_text]

[chara_show  name="圣子"  time="1000"  wait="true"  left="-77"  top="-268"  width="1394"  height="1008"  reflect="false"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="254"  top="-213"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
不愧是你们，竟然还能逃出来[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
既然已经见到了我，就直接束手就擒把，不然别怪我们不客气。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
士兵！拿下他们！[p]

[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-250"  top="-214"  width=""  height=""  reflect="false"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="197"  top="-189"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
怎么办，你的伤还没好，硬来我们不是对手。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
不对！你是，，，，，[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
你是给我钥匙的呢个人！[p]
[_tb_end_text]

[chara_hide  name="男主"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="168"  top="-225"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
为什么？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
哼，哈哈哈哈哈[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
那就不用废话了，接下来我们来谈谈条件吧。[p]

[_tb_end_text]

[chara_hide  name="莉莉露"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-485"  top="-197"  width=""  height=""  reflect="false"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-188"  top="-225"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
你已经骗过我一次了，现在还想有第二次吗？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
哼，我想你应该知道自己现在的处境，不想活命你们当然可以拒绝，只可惜他了，为了救你做了这么多还是。。。什么也没改变[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
神书上写的一定不容更改，恶魔之女一定会被镇压直到审判之日的到来。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
那日便是幸福与光芒照耀大地的日子，那日便是举国欢庆的日子，那日便是----------------------------------------------------------------[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="-772"  top="-717"  width="2733"  height="1975"  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
我登上大主教位子的日子！！！[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="-172"  top="-226"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
你想做什么[p]
[_tb_end_text]

[chara_show  name="圣子"  time="1000"  wait="true"  left="159"  top="-230"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
当然是得到我应得的[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
老东西在上面待得时间太久了，拿得太多了，太贪心了，整个教会的形象都被他败坏了！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
教会应该迎来改革，在这么下去，就连低等的农夫都不会蠢到给我们一分钱了，神明大人会哭泣的！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
你想要。。。[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
除掉老东西[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
。。。我不明白，他是你的父亲。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
你理解什么叫一人之下万人之上吗?这不有趣，也不值得炫耀，因为还有一个人踩在你的头上！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
废话说得太多了，我们直入主题吧。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
你想要我们干什么？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
什么也不用做[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
从这里出去，大胆的出去，杀人也好，藏起来也好，什么都好。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
没有什么比让恶魔逃出去更加可怕的罪名了。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
这么多年的宣传，人们早就对此有根深蒂固的执念了。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
恶魔的出逃，灾祸的降临，我的生意。。哈哈哈哈哈[p]

[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
多余的事情你们就不用知道了，但是还有一件事。[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
把呢个给我[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
我听不懂你在说什么[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
开启地狱门的钥匙，传说中在恶魔的手中，你不是恶魔吗？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
我从来都不是！！！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
那你是谁？一个普通的小女孩？哈哈哈哈教会的工具？不！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
你是恶魔，你骗不了我！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
我不是！我不知道你在说什么！[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
士兵，看来这里有人搞不清楚情况了[p]
[_tb_end_text]

[glink  color="black"  storage="scene1.ks"  size="20"  text="交出银色宝石"  x="533"  y="232"  width=""  height=""  _clickable_img=""  target="*交出银色宝石分支"  ]
[glink  color="black"  storage="scene1.ks"  size="20"  text="挡在她前面"  x="543"  y="315"  width=""  height=""  _clickable_img=""  target="*挡在前面分支"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[s  ]
*交出银色宝石分支

[tb_show_message_window  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-219"  top="-138"  width=""  height=""  reflect="false"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="190"  top="-157"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
给你[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
看来有聪明人[p]
[_tb_end_text]

[chara_hide  name="圣子"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="223"  top="-173"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
这是？[p]
[_tb_end_text]

[tb_start_text mode=1 ]
那天，，，离开的时候我捡到的，钥匙，，，，我看到了，，，[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#リリル
。。。。。。。。。。。。[p]
[_tb_end_text]

[chara_hide  name="莉莉露"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="192"  top="-155"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
钥匙我收到了，你们走吧。我没工夫和你们废话[p]
[_tb_end_text]

[chara_hide_all  time="1000"  wait="true"  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门关闭.jpg"  ]
[chara_show  name="莉莉露"  time="1000"  wait="true"  left="176"  top="-133"  width=""  height=""  reflect="false"  ]
[chara_show  name="男主"  time="1000"  wait="true"  left="-276"  top="-137"  width=""  height=""  reflect="false"  ]
[tb_start_text mode=1 ]
#リリル
你骗了我[p]
[_tb_end_text]

[tb_start_text mode=1 ]
至少我们出去了对吗，对不起[p]
[_tb_end_text]

[tb_start_text mode=1 ]
对不起[p]
[_tb_end_text]

[bg  time="1000"  method="crossfade"  storage="黑地道/门开启.jpg"  ]
[tb_start_text mode=1 ]
只要我们还活着就有机会[p]
[_tb_end_text]

[tb_hide_message_window  ]
[bg  time="1000"  method="crossfade"  storage="黑地道/门开光.jpg"  ]
[chara_hide_all  time="1000"  wait="true"  ]
[s  ]
*挡在前面分支

[bg  time="1000"  method="crossfade"  storage="黑地道/四通八达.jpg"  ]
[chara_show  name="圣子"  time="1000"  wait="true"  left="-184"  top="-241"  width="1440"  height="1040"  reflect="false"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="58"  top="-248"  width="1614"  height="1167"  reflect="false"  ]
[tb_start_text mode=1 ]
#サントリー・オリス
就这样吧，我还有很多办法，看来我确实无法和蠢货交流[p]
[_tb_end_text]

[tb_start_text mode=1 ]
#サントリー・オリス
士兵！动手！[p]
[_tb_end_text]

[chara_hide  name="圣子"  time="1000"  wait="true"  pos_mode="true"  ]
[tb_hide_message_window  ]
[chara_hide  name="士兵"  time="1000"  wait="true"  pos_mode="true"  ]
[chara_show  name="士兵"  time="1000"  wait="true"  left="-1135"  top="-757"  width="3514"  height="2538"  reflect="false"  ]
[s  ]
